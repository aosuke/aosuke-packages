#!/usr/bin/env bash

# This script is used to help spotting badly packaged software.

# This pipeline checks for components not un /usr/local/aosuke
# It is okay though to have /usr and /usr/local , since they are the leading
# components, but anything else than these two paths is bad.

pacman -Ql | cut -d' ' -f2 | sort | uniq \
	| grep -v '^/usr/local/aosuke' \
	| grep -v '^/usr/local/$' \
	| grep -v '^/usr/$' \
	| less
