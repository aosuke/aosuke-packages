aosuke
======

Aosuke is a unix-like distribution meant to be embedded into another unix-like
system. It is using the pacman package manager and toolsets to build binary
packages to install.

It is currently only available for Mac OS X systems. I plan to make it available
for Linux systems and Windows. Windows support should be possible through MSYS2.

It may seem crazy to want to have a common managed subsystem for different 
platforms, but it is not. It will eventually mean that the same idioms can be
used to install software, and a similar, if not functionnally identical, system
will be made available for easier cross-platform work.

Philosophy
----------

*FIXME : Document the philosophy I follow in packaging and not stepping on OSX's toes.*

  * Not replacing system stuff, unless absolutely necessary, and when necessary
    only part of a package, provide `provides` through shim packages for system tools.

Packages structure
------------------

Considerations for splitting PKGBUILDs in folders and repositories. Those
folders will begin appearing here once I have packages to put in them.

  * core/
      * Anything from the archilnux abs core/
  * extra/
      * Anything from the archilnux abs extra/
  * community/
      * Anything from the archilnux abs community/
  * macosx/
      * Mac OS X specific packages, mainly shims for system package provides.
  * macosx-apps/
      * Mac OS X ".app" bundles will be packaged in their own repository.
  * non-free/
      * Software that is non-free (non Open Source Software), though can be packaged
        and re-distributed as packages.
  * no-distribute/
      * This will make available PKGBUILDs to package non-distributable stuff in aosuke.
        None of those built PKGBUILDs should be made available to the public, this is
        mainly for dependencies tracking. These will be PKGBUILDs to package them locally
        by the end-user, a bit like some packages in the AUR.

Repositories
------------

For now, no repositories are available, since the project is still volatile.
I will try as soon as possible to have a binary repository.

The repositories will be structured as the PKGBUILDs are, with one more level
to split packages for OS X releases.

Related projects
----------------

Aosuke will mostly work in a way that will try not to conflict with any other
package management systems on Mac OS X. Sadly, it can not be guaranteed that
another project, or even aosuke, might step on eachothers toes. If ever there
is a conflict with anything from these projects, IT IS A BUG and should be
reported.

##List of similar projects

Here's a list of other projects sharing some, if not all of these goals.

  * [Homebrew](http://brew.sh/), the missing package manager for OS X. Custom tools.
  * [pkgsrc/saveosx](http://saveosx.org/), a port of pkgsrc for Mac OS X, with packages.
  * [funtoo/gentoo prefix](http://www.funtoo.org/Gentoo_Prefix_On_Mac_OS_X), emerge and co from gentoo.
  * [Rudix](http://rudix.org/), custom tools and packages, made for OS X.
  * [fink](http://www.finkproject.org/), dpkg and apt-get, and repository for Mac OS X.
  * [macports](https://www.macports.org/), similar to \*BSD ports systems.

The following projects are mainly for historical purposes. They are, for what
I can tell, abandoned.

  * None that I can tell!

These lists might not be exhaustive. If you know of a similar project, however
small it may be, please request addition here, I'll be glad to link to those
projects, as their recipes might be useful to make improved packages for hard
to package software, even for abandoned projects.

The name
--------

*aosuke (青助)*

What does aosuke mean? Not much, it is the japanese nickname of the blue ghost
in the Pacman game. This distribution uses pacman to manage de packages.
Furthermore, google results were clean of technical stuff when searching for
aosuke, which should make it easier to find answer to technical problems.

There is an additional layer to that name. The character of the ghost is
described as *fickle*. Fickle, as one changing frequently of systems could
be described as.
